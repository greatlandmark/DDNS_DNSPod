package lofit.ddns.dnspod.dnspod_ddns.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lofit.ddns.dnspod.dnspod_ddns.operate.RecordList;

@Configuration
public class Configure {
    @Bean
    public RecordList recordList(){
        System.out.println("配置类");
        return new RecordList();
    }
}