package lofit.ddns.dnspod.dnspod_ddns.credit;

public class HttpProfile {
    
  public static final String REQ_HTTPS = "https://";

  public static final String REQ_HTTP = "http://";

  public static final String REQ_POST = "POST";

  public static final String REQ_GET = "GET";

  /** Time unit, 1 minute, equals 60 seconds. */
  public static final int TM_MINUTE = 60;

  private String reqMethod;

  /** Endpoint means the domain which this request is sent to, such as cvm.tencentcloudapi.com. */
  private String endpoint;

  /** HTTPS or HTTP, currently only HTTPS is valid. */
  private String protocol;

  /** Read timeout in seconds. */
  private int readTimeout;

  /** Write timeout in seconds */
  private int writeTimeout;

  /** Connect timeout in seconds */
  private int connTimeout;

  /** http proxy host */
  private String proxyHost;

  /** http proxy port */
  private int proxyPort;

  /** http proxy user name */
  private String proxyUsername;

  /** http proxy password */
  private String proxyPassword;

  public HttpProfile() {
    this.reqMethod = HttpProfile.REQ_GET;
    this.endpoint = null;
    this.protocol = HttpProfile.REQ_HTTPS;
    this.readTimeout = 0;
    this.writeTimeout = 0;
    this.connTimeout = HttpProfile.TM_MINUTE;
  }

  public String getReqMethod() {
      return reqMethod;
  }

  public void setReqMethod(String reqMethod) {
      this.reqMethod = reqMethod;
  }

  public String getEndpoint() {
      return endpoint;
  }

  public void setEndpoint(String endpoint) {
      this.endpoint = endpoint;
  }

  public String getProtocol() {
      return protocol;
  }

  public void setProtocol(String protocol) {
      this.protocol = protocol;
  }

  public int getReadTimeout() {
      return readTimeout;
  }

  public void setReadTimeout(int readTimeout) {
      this.readTimeout = readTimeout;
  }

  public int getWriteTimeout() {
      return writeTimeout;
  }

  public void setWriteTimeout(int writeTimeout) {
      this.writeTimeout = writeTimeout;
  }

  public int getConnTimeout() {
      return connTimeout;
  }

  public void setConnTimeout(int connTimeout) {
      this.connTimeout = connTimeout;
  }

  public String getProxyHost() {
      return proxyHost;
  }

  public void setProxyHost(String proxyHost) {
      this.proxyHost = proxyHost;
  }

  public int getProxyPort() {
      return proxyPort;
  }

  public void setProxyPort(int proxyPort) {
      this.proxyPort = proxyPort;
  }

  public String getProxyUsername() {
      return proxyUsername;
  }

  public void setProxyUsername(String proxyUsername) {
      this.proxyUsername = proxyUsername;
  }

  public String getProxyPassword() {
      return proxyPassword;
  }

  public void setProxyPassword(String proxyPassword) {
      this.proxyPassword = proxyPassword;
  }

}