package lofit.ddns.dnspod.dnspod_ddns.credit;

public class ClientProfile {

	/** Signature process version 1, with HmacSHA1. */
	public static final String SIGN_SHA1 = "HmacSHA1";

	/** Signature process version 1, with HmacSHA256. */
	public static final String SIGN_SHA256 = "HmacSHA256";

	/** Signature process version 3. */
	public static final String SIGN_TC3_256 = "TC3-HMAC-SHA256";

	private HttpProfile httpProfile;

	private String signMethod;

	/**
	 * If payload is NOT involved in signing process, true means will ignore
	 * payload, default is false.
	 */
	private boolean unsignedPayload;

	public ClientProfile(String signMethod, HttpProfile httpProfile) {
		if (signMethod == null || signMethod.isEmpty()) {
			signMethod = SIGN_SHA256;
		}
		this.unsignedPayload = false;
		this.signMethod = signMethod;
		this.httpProfile = httpProfile;
	}

	public ClientProfile(String signMethod) {
		this(signMethod, new HttpProfile());
	}

	public ClientProfile() {
		this(ClientProfile.SIGN_SHA256, new HttpProfile());
	}

	public HttpProfile getHttpProfile() {
		return httpProfile;
	}

	public void setHttpProfile(HttpProfile httpProfile) {
		this.httpProfile = httpProfile;
	}

	public String getSignMethod() {
		return signMethod;
	}

	public void setSignMethod(String signMethod) {
		this.signMethod = signMethod;
	}

	public boolean isUnsignedPayload() {
		return unsignedPayload;
	}

	public void setUnsignedPayload(boolean unsignedPayload) {
		this.unsignedPayload = unsignedPayload;
	}
}