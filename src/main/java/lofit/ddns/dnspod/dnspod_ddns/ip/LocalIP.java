package lofit.ddns.dnspod.dnspod_ddns.ip;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;

public class LocalIP {
    // InetAddress ip;

    public static String getHostName() {
        try {
            InetAddress ip = InetAddress.getLocalHost();
            return ip.getHostName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String[] getIPv4Addresses() {
        try {
            Enumeration<NetworkInterface> interfs = NetworkInterface.getNetworkInterfaces();
            ArrayList<String> list = new ArrayList<String>();
            while (interfs.hasMoreElements()) {
                NetworkInterface interf = interfs.nextElement();
                Enumeration<InetAddress> addres = interf.getInetAddresses();
                while (addres.hasMoreElements()) {
                    InetAddress in = addres.nextElement();

                    if (in instanceof Inet4Address) {
                        list.add(in.getHostAddress());
                        // System.out.println("v4:" + in.getHostAddress());
                    }
                    // else if (in instanceof Inet6Address) {
                    // System.out.println("v6:" + in.getHostAddress());
                    // }
                }
            }
            return (String[]) list.toArray(new String[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String[] getIPv6Addresses() {
        try {
            Enumeration<NetworkInterface> interfs = NetworkInterface.getNetworkInterfaces();
            ArrayList<String> list = new ArrayList<String>();
            while (interfs.hasMoreElements()) {
                NetworkInterface interf = interfs.nextElement();
                Enumeration<InetAddress> addres = interf.getInetAddresses();
                while (addres.hasMoreElements()) {
                    InetAddress in = addres.nextElement();

                    // if (in instanceof Inet4Address) {
                    // System.out.println("v4:" + in.getHostAddress());
                    // } else
                    if (in instanceof Inet6Address) {
                        list.add(in.getHostAddress());
                        // System.out.println("v6:" + in.getHostAddress());
                    }
                }
            }
            return (String[]) list.toArray(new String[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String[] getAllIPAddresses() {
        try {
            Enumeration<NetworkInterface> interfs = NetworkInterface.getNetworkInterfaces();
            ArrayList<String> list = new ArrayList<String>();
            while (interfs.hasMoreElements()) {
                NetworkInterface interf = interfs.nextElement();
                Enumeration<InetAddress> addres = interf.getInetAddresses();
                while (addres.hasMoreElements()) {
                    InetAddress in = addres.nextElement();
                    list.add(in.getHostAddress());
                    // if (in instanceof Inet4Address) {
                    // System.out.println("v4:" + in.getHostAddress());
                    // } else if (in instanceof Inet6Address) {
                    // System.out.println("v6:" + in.getHostAddress());
                    // }
                }
            }
            return (String[]) list.toArray(new String[list.size()]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getRealIpv4() {
        String localip = null;// 本地IP，如果没有配置外网IP则返回它
        String netip = null;// 外网IP
        try {

            Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
            InetAddress ip = null;
            boolean finded = false;// 是否找到外网IP
            while (netInterfaces.hasMoreElements() && !finded) {
                NetworkInterface ni = netInterfaces.nextElement();
                Enumeration<InetAddress> address = ni.getInetAddresses();
                while (address.hasMoreElements()) {
                    ip = address.nextElement();
                    if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress() && ip.getHostAddress().indexOf(":") == -1) {// 外网IP
                        netip = ip.getHostAddress();
                        finded = true;
                        break;
                    } else if (ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
                            && ip.getHostAddress().indexOf(":") == -1) {// 内网IP
                        localip = ip.getHostAddress();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netip != null && !"".equals(netip)) {

            return netip;
        } else {

            return localip;
        }
    }

    public static String getRealIpv6() {
        String localip = null;// 本地IP，如果没有配置外网IP则返回它
        String netip = null;// 外网IP
        try {

            Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
            InetAddress ip = null;
            boolean finded = false;// 是否找到外网IP
            while (netInterfaces.hasMoreElements() && !finded) {
                NetworkInterface ni = netInterfaces.nextElement();
                Enumeration<InetAddress> address = ni.getInetAddresses();
                while (address.hasMoreElements()) {
                    ip = address.nextElement();
                    if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress() && ip.getHostAddress().indexOf(".") == -1) {// 外网IP
                        netip = ip.getHostAddress();
                        finded = true;
                        int index = netip.indexOf('%');
                        if (index > 0) {
                            netip = netip.substring(0, index);
                        }
                        break;
                    } else if (ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
                            && ip.getHostAddress().indexOf(".") == -1) {// 内网IP
                        localip = ip.getHostAddress();
                        int index = localip.indexOf('%');
                        if (index > 0) {
                            localip = localip.substring(0, index);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netip != null && !"".equals(netip)) {

            return netip;
        } else {

            return localip;
        }
    }
}