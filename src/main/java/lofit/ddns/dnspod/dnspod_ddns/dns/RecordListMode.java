package lofit.ddns.dnspod.dnspod_ddns.dns;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;

import lofit.ddns.dnspod.dnspod_ddns.beans.RecordListBean;
import lofit.ddns.dnspod.dnspod_ddns.credit.AbstractModel;

public class RecordListMode extends AbstractModel {
    @Autowired
    RecordListBean rl;

    @Override
    protected void toMap(HashMap<String, String> map, String prefix) {
        this.setParamSimple(map, "domain", rl.getDomain());
        this.setParamSimple(map, "offset", rl.getOffset());
        this.setParamSimple(map, "length", rl.getLength());
        this.setParamSimple(map, "subDomain",rl.getSubDomain());
        this.setParamSimple(map, "recordType", rl.getRecordType());
        this.setParamSimple(map, "qProjectId",rl.getqProjectId());
    }

}