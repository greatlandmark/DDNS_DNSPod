package lofit.ddns.dnspod.dnspod_ddns.dns;

import lofit.ddns.dnspod.dnspod_ddns.credit.AbstractClient;
import lofit.ddns.dnspod.dnspod_ddns.credit.AbstractModel;
import lofit.ddns.dnspod.dnspod_ddns.credit.ClientProfile;
import lofit.ddns.dnspod.dnspod_ddns.credit.Credential;
import lofit.ddns.dnspod.dnspod_ddns.exception.TencentCloudApiException;

public class RecordListClient extends AbstractClient {
    private static String endpoint = "cns.api.qcloud.com";
    private static String path = "/v2/index.php";
    private static String version = "2017-03-12";
    private static String region = null;
    private static String actionName = "RecordList";

    public RecordListClient(Credential credential, ClientProfile profile) {
        super(endpoint, path, version, credential, region);
        // TODO Auto-generated constructor stub
    }

    public RecordListClient(String endpoint, String version, Credential credential, String region) {
        super(endpoint, path, version, credential, region);
        // TODO Auto-generated constructor stub
    }

    public RecordListClient(String endpoint, String version, Credential credential, String region,
            ClientProfile profile) {
        super(endpoint, path, version, credential, region, profile);
    }

    public void showRecordList(AbstractModel request) {
        try {
            String str=this.internalRequest(request, actionName);
            System.out.println(str);
        } catch (TencentCloudApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}