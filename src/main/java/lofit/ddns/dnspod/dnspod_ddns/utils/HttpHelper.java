package lofit.ddns.dnspod.dnspod_ddns.utils;

import java.io.IOException;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.*;
import java.net.http.HttpResponse.BodyHandlers;
import java.security.URIParameter;
import java.time.Duration;
import java.util.ArrayList;

public class HttpHelper {
    public static void method() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newBuilder().version(Version.HTTP_1_1).followRedirects(Redirect.NORMAL)
                .connectTimeout(Duration.ofSeconds(20)).build();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create("https://arkf.superclass.xyz/"))
                .timeout(Duration.ofMinutes(2)).GET().build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        System.out.println(response.statusCode());
        System.out.println(response.body());
    }

    void doGET() {
        String root = "https://cns.api.qcloud.com/v2/index.php";
        String[] params = new String[] { "Content-Type", "application/json",
         "", "", 
         "", "", 
         "", "", 
         "", "", 
         "", "" };
        String link = root + "?" + paramsToString(params);
        URI uri = URI.create(link);

        try {
            HttpClient client = HttpClient.newBuilder().version(Version.HTTP_1_1).followRedirects(Redirect.NORMAL)
                    .connectTimeout(Duration.ofSeconds(20)).build();
            HttpRequest request = HttpRequest.newBuilder().uri(uri).timeout(Duration.ofMinutes(2))
                    .header("Content-Type", "application/json").GET().build();

            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
            System.out.println(response.statusCode());
            System.out.println(response.body());
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    String paramsToString(String... params) {
        java.util.Objects.requireNonNull(params);
        if (params.length == 0 || params.length % 2 != 0) {
            System.out.println("paramsToString(String ...params) params.length wrong:" + params.length);
            return "";
        }
        StringBuffer s=new StringBuffer();
        for (int i = 0; i < params.length; i += 2) {
            s.append(params[i]);
            s.append("=");
            s.append(params[i + 1]);
            s.append("&");
        }
        return s.toString();
    }
}