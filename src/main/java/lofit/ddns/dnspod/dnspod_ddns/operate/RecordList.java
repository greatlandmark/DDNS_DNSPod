package lofit.ddns.dnspod.dnspod_ddns.operate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import lofit.ddns.dnspod.dnspod_ddns.beans.CommonBean;
import lofit.ddns.dnspod.dnspod_ddns.credit.ClientProfile;
import lofit.ddns.dnspod.dnspod_ddns.credit.Credential;
import lofit.ddns.dnspod.dnspod_ddns.credit.HttpProfile;
import lofit.ddns.dnspod.dnspod_ddns.dns.RecordListClient;
import lofit.ddns.dnspod.dnspod_ddns.dns.RecordListMode;

/**
 * @author Lofit LUO 修改子域名的ip，绑定为现在主机的ipv6.
 */

public class RecordList {
    @Autowired
    CommonBean cb;

    public RecordList() {
        run();
    }

    public void run() {
        try {
            Credential cred = new Credential(cb.getSecretId(), cb.getSecretKey());

            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setReadTimeout(60);
            httpProfile.setReqMethod("GET");
            // httpProfile.setEndpoint(endpoint);

            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setSignMethod(cb.getSignatureMethod());
            clientProfile.setHttpProfile(httpProfile);

            RecordListClient recordListClient = new RecordListClient(cred, clientProfile);

            RecordListMode rlm = new RecordListMode();

            recordListClient.showRecordList(rlm);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

}