package lofit.ddns.dnspod.dnspod_ddns.beans;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties()
public class RecordModifyBean {
    // common
    String secretId;
    String secretKey;
    //一个URL，例如 https://cns.api.qcloud.com/v2/index.php，
    String endpoint;

    // modify

    String action;
    String domain;
    String recordId;
    String subDomain;
    String recordType;
    String recordLine;
    String value;
    String ttl;
    String mx;
    
    String region;
    String signatureMethod;
    String token;

}