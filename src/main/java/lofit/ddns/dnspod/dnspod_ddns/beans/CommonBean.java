package lofit.ddns.dnspod.dnspod_ddns.beans;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "common")
public class CommonBean {
    // common
    String secretId;
    String secretKey;
    //一个URL，例如 https://cns.api.qcloud.com/v2/index.php，
    String endpoint;
    String action;
    String region;
    String signatureMethod;
    String token;

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSignatureMethod() {
        return signatureMethod;
    }

    public void setSignatureMethod(String signatureMethod) {
        this.signatureMethod = signatureMethod;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "CommonBean [action=" + action + ", endpoint=" + endpoint + ", region=" + region + ", secretId="
                + secretId + ", secretKey=" + secretKey + ", signatureMethod=" + signatureMethod + ", token=" + token
                + "]";
    }

}