package lofit.ddns.dnspod.dnspod_ddns.beans;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "record-list")
public class RecordListBean {
    String domain;
    String offset;
    String length;
    String subDomain;
    String recordType;
    String qProjectId;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getSubDomain() {
        return subDomain;
    }

    public void setSubDomain(String subDomain) {
        this.subDomain = subDomain;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getqProjectId() {
        return qProjectId;
    }

    public void setqProjectId(String qProjectId) {
        this.qProjectId = qProjectId;
    }

    @Override
    public String toString() {
        return "RecordListBean [domain=" + domain + ", length=" + length + ", offset=" + offset + ", qProjectId="
                + qProjectId + ", recordType=" + recordType + ", subDomain=" + subDomain + "]";
    }
}