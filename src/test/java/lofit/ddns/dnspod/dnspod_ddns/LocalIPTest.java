package lofit.ddns.dnspod.dnspod_ddns;

import org.junit.jupiter.api.Test;

import lofit.ddns.dnspod.dnspod_ddns.ip.LocalIP;

public class LocalIPTest {
    @Test
    public void demo1() {
        var a = LocalIP.getAllIPAddresses();
        var b = LocalIP.getIPv4Addresses();
        var c = LocalIP.getIPv6Addresses();
        System.out.println(LocalIP.getHostName() + "\n======================\n");
        printS(a);
        System.out.println("\n==============\n");
        printS(b);
        System.out.println("\n==============\n");
        printS(c);
        System.out.println( "\n======================\n"+LocalIP.getRealIpv4());
        System.out.println( "\n======================\n"+LocalIP.getRealIpv6());
    }

    void printS(String[] s) {
        for (int i = 0; i < s.length; i++)
            System.out.println(s[i]);
    }
}